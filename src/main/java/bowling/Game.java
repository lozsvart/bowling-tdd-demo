package bowling;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Game {

    public static final int NUMBER_OF_FRAMES = 10;

    private List<Frame> frames = new LinkedList<>();

    public void roll(int pins) {
        if (getLastFrame()
                .map(Frame::isFull)
                .orElse(true)) {
            Frame previousFrame = getLastFrame().orElse(null);
            frames.add(new Frame(previousFrame, frames.size() == NUMBER_OF_FRAMES - 1));
        }
        getLastFrame()
                .ifPresent(frame -> frame.add(pins));
    }

    private Optional<Frame> getLastFrame() {
        if (frames.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(frames.get(frames.size() - 1));
        }
    }

    public int score() {
        return frames.stream()
                .map(frame -> frame.getBonusScore() + frame.getBaseScore())
                .reduce(0, Integer::sum);
    }

    private static class Frame {

        private final Frame previousFrame;
        private final boolean isLastFrame;
        private List<Integer> rolls = new LinkedList<>();
        private List<Integer> bonusRolls = new LinkedList<>();

        public Frame(Frame previousFrame, boolean isLastFrame) {
            this.previousFrame = previousFrame;
            this.isLastFrame = isLastFrame;
        }

        public void add(int roll) {
            rolls.add(roll);
            propagateBonus(roll);
        }

        private void addBonus(int roll) {
            if (isStrike() && bonusRolls.size() < 2) {
                bonusRolls.add(roll);
                propagateBonus(roll);
            }
            if (isSpare() && bonusRolls.size() < 1) {
                bonusRolls.add(roll);
                propagateBonus(roll);
            }
        }

        private void propagateBonus(int roll) {
            if (previousFrame != null) {
                previousFrame.addBonus(roll);
            }
        }

        public int getBaseScore() {
            return rolls.stream()
                    .reduce(0, Integer::sum);
        }

        public boolean isFull() {
            if (!isLastFrame) {
                return isStrike() || rolls.size() == 2;
            } else {
                if (isStrike() || isSpare()) {
                    return rolls.size() == 3;
                }
                return rolls.size() == 2;
            }
        }

        private boolean isStrike() {
            if (!isLastFrame) {
                return rolls.size() == 1 && rolls.get(0) == 10;
            } else {
                return rolls.get(0) == 10;
            }
        }

        public boolean isSpare() {
            return rolls.size() == 2 && getBaseScore() == 10;
        }

        public Integer getBonusScore() {
            return bonusRolls.stream()
                    .reduce(0, Integer::sum);
        }
    }

}
