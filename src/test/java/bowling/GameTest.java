package bowling;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GameTest {

    private Game game;

    @BeforeEach
    void setUp() {
        game = new Game();
    }

    @Test
    void gutterGameShouldScoreZero() {

        rollMany(0, 20);

        assertEquals(0, game.score());

    }

    @Test
    void allOnesShouldScoreTwenty() {

        rollMany(1, 20);

        assertEquals(20, game.score());

    }

    @Test
    void shouldCalculateSpareBonus() {

        game.roll(4);
        game.roll(6);
        game.roll(3);
        // spare bonus: 3 in frame 1

        rollMany(1, 17);

        assertEquals((4 + 6 + 3) + (3 + 1) + 8 * 2, game.score());

    }

    @Test
    void shouldCalculateStrikeBonus() {

        game.roll(10);
        game.roll(2);
        game.roll(3);

        rollMany(1, 16);

        assertEquals((10 + 2 + 3) + (2 + 3) + 8 * 2, game.score());
    }

    @Test
    void perfectGameShouldScore300() {

        rollMany(10, 12);

        assertEquals(300, game.score());
    }

    private void rollMany(int pins, int times) {
        for (int i = 0; i < times; i++) {
            game.roll(pins);
        }
    }


}
